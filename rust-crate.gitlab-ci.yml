# GitLab CI template for building and testing Rust crates.
#
# Comprehensive CI configuration for Rust crates. Builds and tests a Rust crate in 4 stages.
#
# 1. check stage: runs quick checks (formatting, clippy) on the project.
# 2. test stage: builds runs tests using the default (`stable`), `beta` and `nightly` toolchain.
# 3. build stage: builds documentation (add your release build jobs here).
# 4. deploy stage: deploys documentation to GitLab pages (add your deploy jobs here).
#
# Behavior can be customized by overriding variables. If builds require special dependencies, add
# a `before_script` statement, either globally or to a specific job.
#
# To use this template, include it in your project's CI configuration (`.gitlab-ci.yml`) like this:
#
# ```yaml
# include:
#   - project: zargony/gitlab-ci
#     file: rust-crate.gitlab-ci.yml
# ```

include:
  - local: rust.gitlab-ci.yml

# Reorder default stages to first check and test, then build and deploy.
# Additional stages (e.g. integration testing) can be added by higher level configuration.
stages:
  - check
  - test
  - build
  - deploy

variables:
  # Whether to do additional strict checks and deny warnings and wrongly formatted code.
  RUST_CHECK_STRICT: "true"
  # Whether to build and run tests using the default toolchain version (whatever is specified in
  # a `rust-toolchain` file or `stable` by default).
  RUST_TEST_DEFAULT: "true"
  # Whether to build and run tests using the most recent `beta` toolchain version.
  RUST_TEST_BETA: "true"
  # Whether to build and run tests using the most recent `nightly` toolchain version.
  RUST_TEST_NIGHTLY: "false"
  # Whether to build documentation and deploy master documentation to GitLab pages.
  RUST_BUILD_DOCS: "true"
  # Target to redirect to when the main GitLab page is viewed. This is usually a relative path
  # that points to the index page of a crate's documentation (e.g. `mycrate/index.html`).
  PAGES_REDIRECT: ""

# --- only variables above should be overridden for configuration when including this template ---

# We're trying to avoid using `before_script` and `after_script` in the below jobs, so that files
# that include this template can add these keys without disrupting anything.

# STAGE: check
#
# Do basic checks and try to complete as quickly as possible. Some more strict checks are only
# done if `RUST_CHECK_STRICT` is set (which is the default).

check:
  stage: check
  extends: .rust:check

check:format:
  stage: check
  extends: .rust:format
  only:
    variables:
      - $RUST_CHECK_STRICT == "true"

check:clippy:
  stage: check
  extends: .rust:clippy
  only:
    variables:
      - $RUST_CHECK_STRICT == "true"

# STAGE: test
#
# Build and run all unit tests. Uses whatever toolchain version the project specifies (`stable` by
# default). Also build and test using the most current beta and nightly toolchain versions, but
# ignore failure with nightly (as feedback whether the project builds with the most recent nightly).

test:
  stage: test
  extends: .rust:test
  only:
    variables:
      - $RUST_TEST_DEFAULT == "true"

test:beta:
  stage: test
  extends: .rust:test
  variables:
    RUSTUP_TOOLCHAIN: beta
  only:
    variables:
      - $RUST_TEST_BETA == "true"
  allow_failure: true

test:nightly:
  stage: test
  extends: .rust:test
  variables:
    RUSTUP_TOOLCHAIN: nightly
  cache: {}
  only:
    variables:
      - $RUST_TEST_NIGHTLY == "true"
  allow_failure: true

# STAGE: build
#
# By default this contains only one job for building documentation pages. If you want to do release
# builds, you can add jobs to this stage (e.g. by using `docker-app.gitlab-ci.yml`).

build:doc:
  stage: build
  extends: .rust:doc
  only:
    variables:
      - $RUST_BUILD_DOCS == "true"

# STAGE: deploy
#
# By default this contains only one job for publishing documentation pages to GitLab pages. If you
# want to do automatic deployment, you can add jobs to this stage.

# unfortunately, this job has to be named `pages` and can't be named `deploy:doc`
pages:
  stage: deploy
  dependencies:
    - build:doc
  script:
    - mv target/doc public
    - if [ -n "${PAGES_REDIRECT}" ]; then
        echo '<meta http-equiv="refresh" content="0; url=${PAGES_REDIRECT}">' > public/index.html;
      fi
  only:
    refs:
      - master
    variables:
      - $RUST_BUILD_DOCS == "true"
  artifacts:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-docs
    paths:
      - public/
