# GitLab CI templates for Rust crates and Docker images

CI pipeline configuration (`.gitlab-ci.yml`) usually needs to be done separately for each new project. However, many tasks in a CI pipeline are similar across projects. This repository provides CI templates for projects that use Rust and/or Docker. The templates can be easily pulled into your own projects without duplicating them by using `include` statements in your `.gitlab-ci.yml`.

## CI Pipeline Stages

A typical CI pipeline consists of multiple stages. Stages are executed sequentially, with each stage beginning after the previous one succeeded. A stage can contain multiple jobs that are run in parallel.

1. `check` stage: does quick checks like lint and style checks and is intended to give a fast response.
1. `test` stage: builds all targets (in debug mode) and runs all unit tests.
1. `build` stage: builds all targets with release optimizations and packages it.
1. `integration` stage: use the built package together with other packages to run integration tests.
1. `release` stage: release the built package.
1. optional: wait for manual confirmation to deploy.
1. `deploy` stage: send the built package to the runtime environment.

Essentially a project is first unit-tested, then built/packaged once and the same package is used for integration testing and deployment.

## Rust

The Rust CI configuration template contains everything that is needed to automatically test a crate and build its documentation. To use it, make sure your project has a `Cargo.toml` file in its top directory and add the following to `.gitlab-ci.yml` of your project:

```yaml
include:
  - project: zargony/gitlab-ci
    file: rust-crate.gitlab-ci.yml
```

Using this template will:

- add jobs to the `check` stage that run `cargo fmt`, `cargo check` and `cargo clippy`.
- add jobs to the `test` stage that run `cargo test` using the default Rust toolchain version (stable or whatever your rust-toolchain file specifies), but also using beta and nightly Rust toolchain versions (nightly is allowed to fail). This provides early feedback whether a project will work with upcoming Rust versions.
- add a job to the `build` stage that runs `cargo doc` to build the crate documentation.
- add a job to the `deploy` stage that deploys the crate documentation to GitLab pages.

This job template will properly handle projects which contain a `rust-toolchain` file to set the default Rust toolchain. If `rust-toolchain` exists, the toolchain version specified by it will be used, `stable` otherwise. Jobs may override the toolchain, e.g. the `build:nightly` job forces using the latest `nightly` toolchain.

Cargo and Git are set up for GitLab authentication (using the GitLab CI token), so it is possible to fetch dependencies or submodules from other repositories with limited access using `https`.

There are various CI variables that can be changed to adjust the behavior of Rust jobs added by this template. See the description in `rust-crate.gitlab-ci.yml` for more information.

## Docker

The Docker CI configuration template contains everything that is needed to build a Docker image and push it to a registry. To use it, make sure your project has a `Dockerfile` file in its top directory and add the following to `.gitlab-ci.yml` of your project:

```yaml
include:
  - project: zargony/gitlab-ci
    file: docker-app.gitlab-ci.yml
```

Using this template will:

- add a job to the `build` stage that runs `docker build` to build the Docker image and `docker push` to push it to the registry. The Docker image is tagged with the shortened commit hash, which makes it easy to find the source that an image has been built from and vice versa. Additionally, the image is tagged with the branch or tag name it was built from.
- add a job to the `release` stage that tags the Docker image with `latest` if it was built from the `master` branch. This is intentionally a separate step in a separate stage so that a project can insert an additional stage between the `build` and `release` stages, e.g. for running integration tests based on the built image.

This template will use the container registry of the same GitLab instance by default. Images are named using the project's group and name so that they appear in the project's container registry by default.

There are various CI variables that can be changed to adjust the behavior of Docker jobs added by this template. See the description in `docker-app.gitlab-ci.yml` for more information.

## Integration Testing

Stages `build` and `release` are intentionally kept separate so that a project can insert one or multiple additional stages inbetween them for running integration tests. Integration tests are supposed to be done using the image built in the `build` stage (referenced by the shortened commit hash). Using the same image for integration testing that eventually gets deployed makes it possible that integration tests are meaningful. After integration testing passes, the image gets promoted to a release tag (`latest` for images built from the `master` branch) and can be deployed.

## Deployment

Deploying an application after it has passed all stages of building and testing can be added as a job to the `deploy` stage. Since images that are built from the `master` branch get tagged `latest` only after passing all tests, all that is needed to deploy is triggering the runtime environment to pull and run the latest image.

## Further information

For a comprehensive reference of GitLab CI configuration, see the [Gitlab CI/CD Pipeline Configuration
Reference](https://docs.gitlab.com/ee/ci/yaml/).
