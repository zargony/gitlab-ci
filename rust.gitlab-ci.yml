# GitLab CI template for building and testing Rust crates.
#
# To use, include this template in your project's CI configuration and extend your own jobs from
# these job templates. These are lower level building blocks which provide useful predefined jobs
# for building and testing Rust crates. For a more comprehensive CI configuration for Rust crates,
# see `rust-crate.gitlab-ci.yml`.

variables:
  # Additional Debian APT packages to install before building. These packages will be installed for
  # all jobs except formatting checks which usually don't need them.
  # Set to contents of RUST_EXTRA_APT_PACKAGES by default for backward compatibility.
  RUST_APT_PACKAGES: ${RUST_EXTRA_APT_PACKAGES}
  # Arguments for running clippy checks. By default, all warnings are denied.
  RUST_CLIPPY_ARGS: "-Dwarnings -Drust-2018-idioms -Adeprecated"
  # Additional build arguments for building Rust targets.
  RUST_BUILD_ARGS: ""
  # Set to force using the specified Rust toolchain version (even overrides a `rust-toolchain` file
  # in the project). By default, this is empty so that `rust-toolchain` chooses the toolchain
  # version or `stable` is used as the default.
  RUST_TOOLCHAIN: ""

# --- only variables above should be overridden for configuration when including this template ---

# We're trying to avoid using `before_script` and `after_script` in the below jobs, so that files
# that include this template can add these keys without disrupting anything.

# Base Rust job configuration
.rust:base: &base
  image: rust
  # Set up caching. GitLab can only cache files/directories inside the project directory. By default,
  # we use caching per branch and try to cache downloaded dependencies (`CARGO_HOME` points to
  # `.cache` directory) and compiled object files (`target` directory is cached).
  cache:
    # Use caching per branch. Increase the version prefix if cache keying changes.
    key: rust-v1-${CI_COMMIT_REF_SLUG}
    paths:
      - .cache/
      - target/
  variables:
    # Set cargo home to project directory, so that dependencies can be cached.
    CARGO_HOME: ${CI_PROJECT_DIR}/.cache/cargo
    # Configure cargo to use git cli because libgit2 has auth issues on occasion (rust-lang/cargo#3487).
    CARGO_NET_GIT_FETCH_WITH_CLI: "true"
    # Set rustup toolchain to use
    RUSTUP_TOOLCHAIN: ${RUST_TOOLCHAIN}
  # TODO: it would be nice to have common script steps here, but these couldn't be appended to
  # script:
  #   # Show the active toolchain (may download the toolchain first if different from `stable`)
  #   - rustup show active-toolchain
  #   # Display version information
  #   - rustup --version; cargo --version; rustc --version
  #   # Add git authentication to allow fetching dependencies from non-public repositories
  #   - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
  #   - git config --global credential.helper 'store --file ~/.git-credentials'
  #   # Install additional Debian APT packages
  #   - if [ -n "${RUST_APT_PACKAGES}" ]; then
  #       apt-get update &&
  #       apt-get install -y --no-install-recommends ${RUST_APT_PACKAGES};
  #     fi

# Run checks on source
.rust:check:
  <<: *base
  script:
    # Show the active toolchain (may download the toolchain first if different from `stable`)
    - rustup show active-toolchain
    # Display version information
    - rustup --version; cargo --version; rustc --version
    # Add git authentication to allow fetching dependencies from non-public repositories
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    - git config --global credential.helper 'store --file ~/.git-credentials'
    # Install additional Debian APT packages
    - if [ -n "${RUST_APT_PACKAGES}" ]; then
        apt-get update &&
        apt-get install -y --no-install-recommends ${RUST_APT_PACKAGES};
      fi
    # Run checks
    - cargo check --all --all-targets --all-features

# Run formatting checks on source
.rust:format:
  <<: *base
  script:
    # Show the active toolchain (may download the toolchain first if different from `stable`)
    - rustup show active-toolchain
    # Display version information
    - rustup --version; cargo --version; rustc --version
    # Add git authentication to allow fetching dependencies from non-public repositories
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    - git config --global credential.helper 'store --file ~/.git-credentials'
    # Install rustfmt component
    - rustup component add rustfmt
    # Run formatting checks
    - cargo fmt --all -- --check

# Run clippy checks on source
.rust:clippy:
  <<: *base
  script:
    # Show the active toolchain (may download the toolchain first if different from `stable`)
    - rustup show active-toolchain
    # Display version information
    - rustup --version; cargo --version; rustc --version
    # Add git authentication to allow fetching dependencies from non-public repositories
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    - git config --global credential.helper 'store --file ~/.git-credentials'
    # Install additional Debian APT packages
    - if [ -n "${RUST_APT_PACKAGES}" ]; then
        apt-get update &&
        apt-get install -y --no-install-recommends ${RUST_APT_PACKAGES};
      fi
    # Install clippy component
    - rustup component add clippy
    # Run clippy checks
    - cargo clippy --all --all-targets --all-features -- ${RUST_CLIPPY_ARGS}

# Build all packages and targets
.rust:build:
  <<: *base
  script:
    # Show the active toolchain (may download the toolchain first if different from `stable`)
    - rustup show active-toolchain
    # Display version information
    - rustup --version; cargo --version; rustc --version
    # Add git authentication to allow fetching dependencies from non-public repositories
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    - git config --global credential.helper 'store --file ~/.git-credentials'
    # Install additional Debian APT packages
    - if [ -n "${RUST_APT_PACKAGES}" ]; then
        apt-get update &&
        apt-get install -y --no-install-recommends ${RUST_APT_PACKAGES};
      fi
    # Build everything
    - cargo build --all --all-targets ${RUST_BUILD_ARGS}
  artifacts:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_JOB_NAME}
    paths:
      - Cargo.lock
      - target/

# Build documentation
.rust:doc:
  <<: *base
  script:
    # Show the active toolchain (may download the toolchain first if different from `stable`)
    - rustup show active-toolchain
    # Display version information
    - rustup --version; cargo --version; rustc --version
    # Add git authentication to allow fetching dependencies from non-public repositories
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    - git config --global credential.helper 'store --file ~/.git-credentials'
    # Install additional Debian APT packages
    - if [ -n "${RUST_APT_PACKAGES}" ]; then
        apt-get update &&
        apt-get install -y --no-install-recommends ${RUST_APT_PACKAGES};
      fi
    # Build documentation
    - cargo doc --all --no-deps --all-features
  artifacts:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_JOB_NAME}
    paths:
      - target/doc

# Build everything and run all unit tests
.rust:test:
  <<: *base
  script:
    # Show the active toolchain (may download the toolchain first if different from `stable`)
    - rustup show active-toolchain
    # Display version information
    - rustup --version; cargo --version; rustc --version
    # Add git authentication to allow fetching dependencies from non-public repositories
    - echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}" >> ~/.git-credentials
    - git config --global credential.helper 'store --file ~/.git-credentials'
    # Install additional Debian APT packages
    - if [ -n "${RUST_APT_PACKAGES}" ]; then
        apt-get update &&
        apt-get install -y --no-install-recommends ${RUST_APT_PACKAGES};
      fi
    # Build everything
    - cargo build --all --all-targets
    # Run all unit tests
    - cargo test --all
